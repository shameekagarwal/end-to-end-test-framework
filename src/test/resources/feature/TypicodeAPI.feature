Feature: mock api

  Scenario: get first user i
    When user with id 1 is fetched
    Then user should have name "Leanne Graham" and live on street "Kulas Light"

  Scenario: get first todo i
    When todo with id 1 is fetched
    Then user should have title "delectus aut autem"

  Scenario: get first user ii
    When user with id 1 is fetched
    Then user should have name "Leanne Graham" and live on street "Kulas Light"

  Scenario: get first todo ii
    When todo with id 1 is fetched
    Then user should have title "delectus aut autem"

  Scenario: get first user iii
    When user with id 1 is fetched
    Then user should have name "Leanne Graham" and live on street "Kulas Light"

  Scenario: get first todo iii
    When todo with id 1 is fetched
    Then user should have title "delectus aut autem"

  Scenario: get first user iv
    When user with id 1 is fetched
    Then user should have name "Leanne Graham" and live on street "Kulas Light"

  Scenario: get first todo iv
    When todo with id 1 is fetched
    Then user should have title "delectus aut autem"
