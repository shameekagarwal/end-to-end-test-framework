package com.example.framework.config;

import org.openqa.selenium.remote.RemoteWebDriver;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.context.support.SimpleThreadScope;
import org.springframework.stereotype.Component;

@Component
public class BrowserScopeConfig implements BeanFactoryPostProcessor {

  public static final String SCOPE_BROWSER = "browser";

  private static class BrowserScope extends SimpleThreadScope {

    @Override
    public Object get(String name, ObjectFactory<?> objectFactory) {
      RemoteWebDriver driver = (RemoteWebDriver) super.get(name, objectFactory);
      if (driver.getSessionId() == null) {
        super.remove(name);
        driver = (RemoteWebDriver) super.get(name, objectFactory);
      }
      return driver;
    }

    @Override
    public void registerDestructionCallback(String name, Runnable callback) {
    }
  }

  @Override
  public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
    beanFactory.registerScope(SCOPE_BROWSER, new BrowserScope());
  }
}

