package com.example.framework.config;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.Scope;

import java.net.URL;
import java.time.Duration;

@Configuration
public class DriverConfig {

  @Value("${custom.implicit-wait}")
  private Integer implicitWait;

  @Value("${custom.headless}")
  private Boolean headless;

  @Value("${custom.grid-url:}")
  private URL url;

  public void configureDriver(WebDriver driver) {
    driver.manage().window().maximize();
    driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(implicitWait));
  }

  @Bean
  @Scope(BrowserScopeConfig.SCOPE_BROWSER)
  @Profile("remote")
  public WebDriver remoteChromeDriver() {
    WebDriver driver = new RemoteWebDriver(this.url, getChromeOptions());
    configureDriver(driver);
    return driver;
  }

  @Bean
  @Scope(BrowserScopeConfig.SCOPE_BROWSER)
  @Profile("local")
  public WebDriver chromeDriver() {
    WebDriverManager.chromedriver().setup();
    WebDriver driver = new ChromeDriver(getChromeOptions());
    configureDriver(driver);
    return driver;
  }

  public ChromeOptions getChromeOptions() {
    ChromeOptions chromeOptions = new ChromeOptions();
    chromeOptions.setHeadless(headless);
    return chromeOptions;
  }
}
