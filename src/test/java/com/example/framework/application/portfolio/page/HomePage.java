package com.example.framework.application.portfolio.page;

import io.cucumber.spring.ScenarioScope;
import lombok.RequiredArgsConstructor;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
@ScenarioScope
@RequiredArgsConstructor
public class HomePage {

  private final WebDriver webDriver;

  @Value("${custom.portfolio.base-url}")
  private String portfolioBaseUrl;

  private final By heading$ = By.xpath("//h1");

  public void goTo() {
    webDriver.get(portfolioBaseUrl);
  }

  public WebElement getHeading() {
    return webDriver.findElement(heading$);
  }
}
