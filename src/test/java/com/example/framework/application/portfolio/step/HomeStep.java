package com.example.framework.application.portfolio.step;

import com.example.framework.application.portfolio.constant.StateConstants;
import com.example.framework.application.portfolio.page.HomePage;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

@Slf4j
@RequiredArgsConstructor
public class HomeStep {

  @Setter(onMethod = @__({@Autowired}), onParam = @__({@Qualifier("state")}))
  private Map<String, Object> state;
  private final HomePage homePage;

  @When("navigate to home page")
  public void navigateToHomePage() throws Exception {
    Thread.sleep(60000);
    homePage.goTo();
    state.put(StateConstants.HOME_PAGE_HEADING_TEXT, homePage.getHeading().getText());
  }

  @Then("should have text {string}")
  public void shouldHaveText(String heading) {
    assertEquals(heading, state.get(StateConstants.HOME_PAGE_HEADING_TEXT));
  }
}
