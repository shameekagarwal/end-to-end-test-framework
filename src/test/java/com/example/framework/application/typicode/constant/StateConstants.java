package com.example.framework.application.typicode.constant;

public class StateConstants {
  public static final String TODO_RESPONSE = "TODO_RESPONSE";

  public static final String USER_RESPONSE = "USER_RESPONSE";
}
