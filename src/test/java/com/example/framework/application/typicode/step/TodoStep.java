package com.example.framework.application.typicode.step;

import com.example.framework.application.typicode.constant.StateConstants;
import com.example.framework.application.typicode.model.Todo;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

@Slf4j
@RequiredArgsConstructor
public class TodoStep {

  @Setter(onMethod = @__({@Autowired}), onParam = @__({@Qualifier("state")}))
  private Map<String, Object> state;

  @Value("${custom.typicode.base-url}")
  private String typicodeBaseUrl;

  @When("todo with id {int} is fetched")
  public void todoWithIdIsFetched(Integer id) throws Exception {
    Thread.sleep(60000);
    Response response = RestAssured.given()
        .when()
        .get(typicodeBaseUrl + "/todos/1");
    state.put(StateConstants.TODO_RESPONSE, response);
  }

  @Then("user should have title {string}")
  public void userShouldHaveTitle(String title) {
    Response response = (Response) state.get(StateConstants.TODO_RESPONSE);
    Todo todo = response.as(Todo.class);
    assertEquals(title, todo.getTitle());
  }
}
