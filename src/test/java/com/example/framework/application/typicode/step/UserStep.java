package com.example.framework.application.typicode.step;

import com.example.framework.application.typicode.constant.StateConstants;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;

import java.util.Map;

import static org.hamcrest.Matchers.equalTo;

@Slf4j
@RequiredArgsConstructor
public class UserStep {

  @Setter(onMethod = @__({@Autowired}), onParam = @__({@Qualifier("state")}))
  private Map<String, Object> state;

  @Value("${custom.typicode.base-url}")
  private String typicodeBaseUrl;

  @When("user with id {int} is fetched")
  public void userWithIdIsFetched(Integer id) throws Exception {
    Thread.sleep(60000);
    Response response = RestAssured.given()
        .when()
        .get(typicodeBaseUrl + "/users/" + id);
    state.put(StateConstants.USER_RESPONSE, response);
  }

  @Then("user should have name {string} and live on street {string}")
  public void userShouldHaveNameAndLiveOnStreet(String name, String street) {
    Response response = (Response) state.get(StateConstants.USER_RESPONSE);
    response.then()
        .statusCode(HttpStatus.SC_OK)
        .assertThat()
        .body("name", equalTo(name))
        .body("address.street", equalTo(street));
  }
}
