package com.example.framework.application.shared;

import io.cucumber.java.After;
import io.cucumber.java.Scenario;
import lombok.RequiredArgsConstructor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

@RequiredArgsConstructor
public class Hooks {
  private final WebDriver driver;

  @After(value = "@browser")
  public void takeScreenshot(Scenario scenario) {
    // optionally, attach screenshot only when scenario.isFailed()
    byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
    scenario.attach(screenshot, "image/png", scenario.getName());
    driver.quit();
  }
}
