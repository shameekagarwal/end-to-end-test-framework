# End-to-End Test Framework

### Components Used

- cucumber - bdd
- selenium - ui tests
- rest assured - api testing
- spring boot - spring container for dependency injection

### Issues Addressed

- sharing state across multiple step definitions
- running tests in parallel
- handling `WebDriver` in a multithreaded scenario
- a decent reporting mechanism with execution time, screenshots for ui tests, etc
- run on normal browser for running tests locally
- selenium grid for running tests in gitlab pipelines
